# ====================================================================
# Bash functions
# ====================================================================
#
# SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
#
# SPDX-License-Identifier: CC0-1.0
#
# Date created: 2022-02-11
# Keywords:     bash, configs, terminal
# Links:        [repo] <https://gitlab.com/digital-dasein/configs/bash>
#
# Short:        Bash function configs (and function aliases)
#
#---------------------------------------------------------------------
# Overview of all available functions (~aliases):
#
#  - gitlog    [OPTS]  -> gitlog console-format, with relative ts, short hash
#  - gitlog-l  [OPTS]  -> gitlog console-format, with relative ts, long hash
#  - gitlog-d  [OPTS]  -> gitlog console-format, with short date timestamp
#  - gitlog-md [OPTS]  -> gitlog markdown-format (short date)
#
# ====================================================================

#=====================================================================
# General
#=====================================================================

# find and replace recusrively
function find-and-replace () {
  if [ -z ${STR_FIND} ];then
    echo "environmental variable STR_FIND is not defined"
    return
  fi
  if [ -z ${STR_REPLACE} ];then
    echo "[WARN] Environmental variable STR_REPLACE is not defined..."
    read -p "Do you wish to replace with empty string? (y/[n]): " yn
    case $yn in
        [Yy]* ) ;;
        [Nn]* ) return;;
        * ) return;;
    esac
  fi
  echo "Recursively finding '${STR_FIND}' and replacing it with '${STR_REPLACE}'..."
  grep -rl $STR_FIND . | xargs sed -i "s/$STR_FIND/$STR_REPLACE/g"
}

# open program with default app 
function x() {
  xdg-open "${1}"
}

# open program with default app, and redirect to /dev/null
function xx() {
  xdg-open "${1}" > /dev/null 2>&1
}

#=====================================================================
# Git
#=====================================================================

# ref: <https://git-scm.com/docs/git-log>
# for tags, use the ".." syntax
#
# Example commands (same for gitlog-d and gitlog-md)
#
#   $ gitlog                    (all logs)
#   $ gitlog v0.0.1..           (everything since tag v0.0.1)
#   $ gitlog v0.0.1..v0.0.2     (everything between tag v0.0.1 and v0.0.2)
#   $ gitlog 2days              (everything of last two days)
#   $ gitlog 2022-02-20         (everything since 2022-02-20)
#   $ gitlog "3 weeks"          (everything of last three weeks)
#   $ gitlog -x "5day"          (everything of last 5 days, copied to cb)
#   $ gitlog -o "--since=2022-02-14 --until=2022-02-20"
#                               (pass custom options with -c or -o; see docs!)
#   $ gitlog | grep -iv "update\|add"
#                               (exclude patterns including udpate or add)
#

# helper function
function gitlog_main () {
  format="$3"
  opts="$4"
  arg="$1"

  xclip=0

  if [ "$1" = "-x" ];then
    xclip=1
    if [ "$2" = "-o" ] || [ "$2" = "-c" ]; then
      echo "gitlog copied to clipboard"
      git log $3 $opts --format=format:"$format" | xclip -sel clip
      return
    else
      arg=$2
    fi
  else
    if [ "$1" = "-o" ] || [ "$1" = "-c" ]; then
      echo "$2"
      git log $2 $opts --format=format:"$format"
      return
    fi
  fi
  if [ -z "$arg" ];then
    if [ "$xclip" = "1" ];then
      echo "gitlog copied to clipboard"
      git log $opts --format=format:"$format" | xclip -sel clip
    else
      git log $opts --format=format:"$format"
    fi
    return
  fi
  # single argument, since X
  #first_char="$(printf '%s' "$arg" | cut -c1)"
  #if [ "$first_char" = "v" ] || [ "$first_char" = "V" ];then
  case "$arg" in
    *..*)
      # version tags
      if [ "$xclip" = "1" ];then
        echo "git history tags '$arg' (copied to clipboard)"
        git log $arg $opts --format=format:"$format" | xclip -sel clip
      else
        git log $arg $opts --format=format:"$format"
      fi
      ;;
    *)
      if [ "$xclip" = "1" ];then
        echo "git history '$arg' (copied to clipboard)"
        git log --since="${arg}" $opts --format=format:"$format" | xclip -sel clip
      else
        git log --since="${arg}" $opts --format=format:"$format"
      fi
      ;;
  esac
  if [ "$2" = "-x" ] || [ "$3" = "-x" ] || \
    [ "$2" = "-c" ] || [ "$3" = "-c" ];then
      echo "[WARNING] clipboard copy option (-x/-c) ignored when not passed as first argument"
  fi
}

function gitlog () {
  # output gitlog to terminal, with relative timestamps
  format="%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)"
  opts="--graph --oneline"
  gitlog_main "$1" "$2" "$format" "$opts"
}

function gitlog-l () {
  # output gitlog to terminal, withs long commit hashes
  format="%C(bold blue)%H%C(reset) - %C(bold green)(%as)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)"
  opts="--graph --oneline"
  gitlog_main "$1" "$2" "$format" "$opts"
}

function gitlog-d () {
  # output gitlog to terminal, withs short dates
  format="%C(bold blue)%h%C(reset) - %C(bold green)(%as)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)"
  opts="--graph --oneline"
  gitlog_main "$1" "$2" "$format" "$opts"
}

function gitlog-md () {
  # output gitlog in markdown format (list)
  format='- %H (**%as**) | %s%  (*%an*)'
  opts="--oneline"
  gitlog_main "$1" "$2" "$format" "$opts"
}
