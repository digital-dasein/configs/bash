# ====================================================================
# Bash aliases
# ====================================================================
#
# SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>
# SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
#
# SPDX-License-Identifier: CC0-1.0
#
# Date created: 2022-02-11
# Keywords:     bash, configs, terminal
# Links:        [repo] <https://gitlab.com/digital-dasein/configs/bash>
#
# Short:        Bash aliases for productive UNIX terminal interactions
#               Note: aliases starting with 'show-' typically aim to provide
#               convenient reminders to some useful commands
#
# ====================================================================

#=====================================================================
# General
#=====================================================================

alias sb='source ~/.bashrc'

# reload bashrc (source bash)
alias br='source ~/.bashrc'
alias sb='source ~/.bashrc'

# ask for rm or cp or mv (escape through -f option)
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias cdd='cd ..'
alias cddd='cd ../..'
alias cdddd='cd ../../..'
alias cddddd='cd ../../../..'

alias duh='du --max-depth=1 -h'
alias up='sudo apt update && sudo apt upgrade'
alias ports="sudo netstat -ntlp | grep LISTEN"

alias l="ls -lh --group-directories-first"
alias ll="ls -lhA --group-directories-first"
alias lll="ls -lhA"

alias c="clear"
alias cc="clear && pwd && ls -lh --group-directories-first"

alias cl="clear && pwd && ls -lh --group-directories-first"
alias cls="clear && ls"

alias e="exit"

alias cd-1='cd  "$(\ls -1dt ./*/ | head -n 1)"'
alias cd-2='cd  "$(\ls -1dt ./*/ | head -n 2 | tail -n 1)"'
alias cd-3='cd  "$(\ls -1dt ./*/ | head -n 3 | tail -n 1)"'
alias cd1='cd  "$(\ls -1dt ./*/ | head -n 1)"'
alias cd2='cd  "$(\ls -1dt ./*/ | head -n 2 | tail -n 1)"'
alias cd3='cd  "$(\ls -1dt ./*/ | head -n 3 | tail -n 1)"'

alias rm-1='rm -ri  "$(\ls -1dt ./*/ | head -n 1)"'
alias rm-2='rm -ri "$(\ls -1dt ./*/ | head -n 2 | tail -n 1)"'
alias rm-3='rm -ri "$(\ls -1dt ./*/ | head -n 3 | tail -n 1)"'
alias rm1='rm -ri  "$(\ls -1dt ./*/ | head -n 1)"'
alias rm2='rm -ri "$(\ls -1dt ./*/ | head -n 2 | tail -n 1)"'
alias rm3='rm -ri "$(\ls -1dt ./*/ | head -n 3 | tail -n 1)"'

alias cp-1='cp -r "$(\ls -1dt ./* | head -n 1 | tail -n 1)" ~/Downloads'
alias cp-2='cp -r "$(\ls -1dt ./* | head -n 2 | tail -n 1)" ~/Downloads'
alias cp-3='cp -r "$(\ls -1dt ./* | head -n 3 | tail -n 1)" ~/Downloads'
alias cp1='cp -r "$(\ls -1dt ./* | head -n 1 | tail -n 1)" ~/Downloads'
alias cp2='cp -r "$(\ls -1dt ./* | head -n 2 | tail -n 1)" ~/Downloads'
alias cp3='cp -r "$(\ls -1dt ./* | head -n 3 | tail -n 1)" ~/Downloads'

# copy current working directory to clipboard (requires x-clip)
alias pwdc='pwd | tr -d "\n" | xclip -selection clipboard'
# with enter
alias pwdce='pwd | xclip -selection clipboard'


# echo public ip address
alias pub='echo $( curl https://ipinfo.io/ip )'
alias ipp='echo $( curl https://ipinfo.io/ip )'

# check current disk capacity
alias dev='df -h | grep nvm'

# shorten cmd line path
alias p0="PROMPT_DIRTRIM=100"
alias p1="PROMPT_DIRTRIM=5"
alias p2="PROMPT_DIRTRIM=3"
alias p3="PROMPT_DIRTRIM=1"

# hostlong
alias hl="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '"

# hostwhite
alias hw="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[00m\]:\[\033[01;38m\]\w\[\033[00m\]\$ '"
alias light="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[00m\]:\[\033[01;38m\]\w\[\033[00m\]\$ '"

# hostblack
alias hb="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '"
alias black="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '"

alias short="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ' && PROMPT_DIRTRIM=1"

alias long="PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ' && PROMPT_DIRTRIM=10"

#=====================================================================
# OPEN FILES
#=====================================================================

alias bp='vi ~/.profile'
alias ba='vi ~/.bash_aliases'
alias brc='vi ~/.bashrc'
alias bp='vi ~/.bash_paths'

alias vr='vi ~/.vimrc'
alias vrc='vi ~/.vimrc'

alias open='xdg-open .'

# open last modified files in a certain dir
alias vi-1='vi `ls -tr | tail -1`'
alias vi-2='vi `ls -tr | tail -2`'
alias vi-3='vi `ls -tr | tail -3`'
alias vi1='vi `ls -tr | tail -1`'
alias vi2='vi `ls -tr | tail -2`'
alias vi3='vi `ls -tr | tail -3`'

# open last modified files with default application
alias o-1='xdg-open "$(\ls -1dt ./* | head -n 1 | tail -n 1)" &'
alias o-2='xdg-open "$(\ls -1dt ./* | head -n 2 | tail -n 1)" &'
alias o-3='xdg-open "$(\ls -1dt ./* | head -n 3 | tail -n 1)" &'
alias o1='xdg-open "$(\ls -1dt ./* | head -n 1 | tail -n 1)" &'
alias o2='xdg-open "$(\ls -1dt ./* | head -n 2 | tail -n 1)" &'
alias o3='xdg-open "$(\ls -1dt ./* | head -n 3 | tail -n 1)" &'
alias x1='xdg-open "$(\ls -1dt ./* | head -n 1 | tail -n 1)" &'
alias x2='xdg-open "$(\ls -1dt ./* | head -n 2 | tail -n 1)" &'
alias x3='xdg-open "$(\ls -1dt ./* | head -n 3 | tail -n 1)" &'

#=====================================================================
# Program related 
#=====================================================================

# open git repo in browser
alias gitopen='git remote -v | awk '"'"'/origin.*push/ {print $2}\'"'"' | sed -e "s/git@gitlab.com:/https:\/\/gitlab.com\//" | sed -e "s/git@gitlab.kuleuven.be:/https:\/\/gitlab.kuleuven.be\//"  |  sed -e "s/git@github.com:/https:\/\/github.com\//"| xargs xdg-open'
alias gopen='gitopen'

#=====================================================================
# Search, find, replace, ... 
#=====================================================================

alias show-find-and-replace='echo grep -rl oldtext . \| xargs sed -i "s/oldtext/newtext/g"'
