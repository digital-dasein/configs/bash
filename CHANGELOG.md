<!--

 SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Gerben Peeters    <gerben@digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

 SPDX-License-Identifier: CC0-1.0

-->

# Changelog

## Release v0.0.2

### Main Features

- Bash functions

### Commits

- 7a4f2111a6b35e68aab1d3ee41611e11721b700a (**2022-03-17**) | Update bash functions (*svbaelen*)
- 32e6a09c493d20d805361c190854255b6b3fb9ae (**2022-02-23**) | Fix markdown list in gitlog function (*svbaelen*)
- 219d325431ed22f3ad4f922a611310ddf807087c (**2022-02-22**) | Main helper function for Git function aliases (*svbaelen*)
- 5649160e05a0299bc4bee620fc57a3f93c62e89e (**2022-02-22**) | add bash functions for git (*svbaelen*)
- 4e6a30381eb62c05b9a3cbb3e537c996080ec421 (**2022-02-22**) | added some clear and exit aliases (*Gerben Peeters*)

## Release v0.0.1

### Main features

- Initial setup

### Commits

- 839ce152be37d581f88acf5f5234b1c0e286841e (**2022-02-16**) | fix rm (*svbaelen*)
- b43afe0f9a7b3913929617073e190c01475a8d76 (**2022-02-16**) | add paths (*svbaelen*)
- b2407dd448d5f48cb373fab5302b9f8354114fe8 (**2022-02-16**) | added basic bash_aliases (*svbaelen*)
- adfdb357ae32cc84bbd999760778c1388adbfbf8 (**2022-02-03**) | Delete .gitlab-ci.yml (*Senne Van Baelen*)
- 95b44c83d00b694b4a3344c17fa7a425f93debd7 (**2022-02-03**) | Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist (*Senne Van Baelen*)

