<!--

 SPDX-FileCopyrightText: 2022 Digital Dasein    <https://digitaldasein.org>
 SPDX-FileCopyrightText: 2022 Senne Van Baelen  <senne@digitaldasein.org>

 SPDX-License-Identifier: CC0-1.0

-->

# Bash configs

- aliases: [./bash_aliases](./bash_aliases) (basic aliases).
- paths: [./bash_paths](./bash_paths) (system path configs)
- functions: [./bash_functions](./bash_functions) (function aliases, extending 
basic aliases)

**NOTE:** Feel free to concatenate all files into one, e.g., into your `bashrc` 
(not recommended for extensive bash configs)

## Usage

Copy, for instance, to your `$HOME` folder (on, or all of them):

```sh
$ cp ./bash_aliases $HOME/.bash_aliases
$ cp ./bash_paths $HOME/.bash_paths
$ cp ./bash_functions $HOME/.bash_functions
$ cp ./bash_svbaelen $HOME/.bash_svbaelen
```


Source them at the appropriate place (if not executed already), e.g. in 
`~/.bashrc`:

```bash
[...]
source ~/.bash_aliases
source ~/.bash_paths
source ~/.bash_functions
source ~/.bash_svbaelen
[...]
```

From the `bash` manpage:

> When bash is invoked as an interactive login shell, or as a non-interactive 
> shell with the `--login option`, it first reads and executes commands from 
> the file `/etc/profile`, if that file exists. After reading that file, it 
> looks for `~/.bash_profile`, `~/.bash_login`, and `~/.profile`, in that 
> order, and reads and executes commands from the first one that exists and is 
> readable.  The `--noprofile` option may be used when the shell is started to 
> inhibit this behavior.
>
>  When a login shell exits, bash reads and executes commands from the file 
>  `~/.bash_logout`, if it exists.
>
> When an interactive shell that is not a login shell is started, bash reads 
> and  executes commands from `~/.bashrc`, if that file exists. This may be 
> inhibited  by using the `--norc` option. The `--rcfile` file option will 
> force bash to read  and execute commands from file instead of `~/.bashrc`.

**Conclusion**: put all of your commands in either `.bashrc` or 
`.bash_profile`, and then have the other file source the first one.

Or **copy content manually** to existing bash configs.
